import webapp


class Content(webapp.webApp):
    content = {'/': 'Main page', '/page': 'This is a page', '/home': 'Welcome to my page'
        , '/about': 'This is about page', '/contact': 'This is contact page'}

    def parse(self, request):

        return request.split(' ', 2)[1]

    def process(self, resourceName):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """

        if resourceName in self.content.keys():
            httpCode = "200 OK"
            htmlBody = "<html><body>" + self.content[resourceName] \
                       + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "Not Found"
        return (httpCode, htmlBody)


if __name__ == '__main__':
    hola = Content('localhost', 1234)
